﻿namespace NetSalaryTaxes.Core.Contracts
{
    public interface IReader
    {
        string ReadLine();
    }
}