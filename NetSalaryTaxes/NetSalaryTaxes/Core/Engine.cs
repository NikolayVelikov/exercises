﻿using NetSalaryTaxes.Core.Contracts;

namespace NetSalaryTaxes.Core
{
    public class Engine : IEngine
    {
        private readonly IWriter writer;
        private readonly IReader reader;

        public Engine(IWriter writer, IReader reader)
        {
            this.writer = writer;
            this.reader = reader;
        }

        public void Run()
        {
            decimal income = decimal.Parse(reader.ReadLine());//Working with decimal because the task is to calculate money

            decimal taxes = 0;
            decimal netSalary = 0;

            if (income <= Constants.minValue)
            {
                netSalary = income;
            }
            else
            {
                taxes = TaxCalculation(income);
                netSalary = income - taxes;
            }

            Print(taxes, netSalary);
        }

        /// <summary>
        /// Print the result according to implementation of the Writer.The Precision is with two symbols after the comma
        /// </summary>
        /// <param name="taxes"></param>
        /// <param name="netSalary"></param>
        private void Print(decimal taxes, decimal netSalary)
        {
            writer.WriteLine(Constants.TAXES + $"{taxes:f2}");
            writer.WriteLine(Constants.NET_SALARY + $"{netSalary:f2}");
        }

        /// <summary>
        /// Distribution of responsibilities for calculating the types of fees.
        /// </summary>
        /// <param name="income"></param>
        /// <returns>The sum of all fees</returns>
        private decimal TaxCalculation(decimal income)
        {
            decimal charge = CalculateIncomeTax(income) + CalculateSocialContribution(income);

            return charge;
        }

        /// <summary>
        /// Calculates income tax
        /// </summary>
        /// <param name="income"></param>
        /// <returns> Tax </returns>
        private decimal CalculateIncomeTax(decimal income)
        {
            return RemoveMinValue(income) * Constants.incomeTax;
        }

        /// <summary>
        /// Calculates social contribution
        /// </summary>
        /// <param name="income"></param>
        /// <returns> Tax of social contribution</returns>
        private decimal CalculateSocialContribution(decimal income)
        {
            if (income >= Constants.maxValue)
            {
                income = Constants.maxValue;
            }

            return RemoveMinValue(income) * Constants.socialTax;
        }

        /// <summary>
        /// Method for removing the min value from income
        /// </summary>
        /// <param name="money"></param>
        /// <returns>Income withoud min value</returns>
        private decimal RemoveMinValue(decimal money)
        {
            return money - Constants.minValue;
        }
    }
}