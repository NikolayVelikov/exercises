﻿namespace NetSalaryTaxes.Core
{
    public class Constants
    {
        public const decimal minValue = 1000;
        public const decimal maxValue = 3000;
        public const decimal incomeTax = 0.1M;
        public const decimal socialTax = 0.15M;

        public const string TAXES = "All taxes: ";
        public const string NET_SALARY = "Net Salary: ";
    }
}