﻿using System;

using NetSalaryTaxes.Core.Contracts;

namespace NetSalaryTaxes.Core
{
    public class ConsoleReader : IReader
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}