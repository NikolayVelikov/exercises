﻿using System;

using NetSalaryTaxes.Core.Contracts;

namespace NetSalaryTaxes.Core
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string text)
        {
            Console.Write(text);
        }

        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}