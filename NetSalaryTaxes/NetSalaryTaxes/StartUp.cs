﻿using NetSalaryTaxes.Core;
using NetSalaryTaxes.Core.Contracts;

namespace NetSalaryTaxes
{
    public class StartUp
    {
        static void Main(string[] args)
        {
            IWriter writer = new ConsoleWriter();
            IReader reader = new ConsoleReader();

            IEngine engine = new Engine(writer, reader);
            engine.Run();
        }
    }
}