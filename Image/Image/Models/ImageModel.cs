﻿using Microsoft.AspNetCore.Http;

namespace Image.Models
{
    public class ImageModel
    {
        public string Title { get; set; }

        public IFormFile File { get; set; }
    }
}
