﻿using Image.DataBase;
using Image.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace Image.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ImageContext context;
        private readonly IWebHostEnvironment env;

        public HomeController(ILogger<HomeController> logger, ImageContext context, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            this.context = context;
            this.env = webHostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult UploadImage(ImageModel model)
        {
            DataBase.Image img = new DataBase.Image();
            img.Title = model.Title;

            //foreach (var file in Request.Form.Files)
            //{
            //img.ImageTitle = file.FileName;

            using (MemoryStream ms = new MemoryStream())
            {
                //file.CopyTo(ms);
                img.Title = model.File.ContentType;
                model.File.CopyTo(ms);
                img.ImageData = ms.ToArray();
            }

            this.context.Images.Add(img);
            this.context.SaveChanges();
            //}

            return View("Index");
        }

        public IActionResult Get(int id)
        {
            var provider = new PhysicalFileProvider(this.env.WebRootPath);
            var content = provider.GetDirectoryContents(Path.Combine("img"));
            var objFile = content.OrderBy(m => m.LastModified);

            string result = string.Empty;
            foreach (var item in objFile.ToList())
            {
                result = @"/img/" + item.Name;
            }

            ViewBag.ImageDataUrl = result;

            return View("Index");
        }

        //public IActionResult Get(int id)
        //{
        //    var image = this.context.Images.FirstOrDefault(x => x.Id == id);
        //    string imageBase64Data = Convert.ToBase64String(image.ImageData);
        //    string imageDataUrl = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
        //    ViewBag.ImageDataUrl = imageDataUrl;

        //    return View("Index");
        //}

    }
}
