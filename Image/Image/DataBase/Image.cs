﻿using System.ComponentModel.DataAnnotations;

namespace Image.DataBase
{
    public class Image
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string ImageTitle { get; set; }

        public byte[] ImageData { get; set; }
    }
}