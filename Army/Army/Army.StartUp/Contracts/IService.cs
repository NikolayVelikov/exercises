﻿using System.Collections.Generic;

using Army.Enumerators;
using Army.Hierarchy.Contracts;

namespace Army.StartUp.Contracts
{
    public interface IService
    {
        void CreatePrivate(int id, string firstName, string lastName, decimal salary);
        void CreateSpy(int id, string firstName, string lastName, int codeNumber);
        void CreateLeutenantGeneral(int id, string firstName, string lastName, decimal salary, List<int> privateIds);
        void CreateEngineer(int id, string firstName, string lastName, decimal salary, Corp corp, List<string> repairInfo);
        void CreateCommando(int id, string firstName, string lastName, decimal salary, Corp corp, List<string> missionInfo);
        void FinishMission(string missionName);
        string Print();
    }
}