﻿using System.Collections.Generic;

using Army.Hierarchy.Contracts;
using Army.Hierarchy.Contracts.IPrivate;

namespace Army.StartUp.Contracts
{
    public interface IRepository
    {
        IReadOnlyCollection<ISoldier> Soldiers { get; }

        void AddSoldier(ISoldier soldier);
        List<IPrivate> ReturnPrivates(IList<int> @privates);
        void AddMission(IMission mission);
        IMission FindMission(string missionName);
    }
}