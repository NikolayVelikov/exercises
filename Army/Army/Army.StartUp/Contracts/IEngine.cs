﻿namespace Army.StartUp.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}