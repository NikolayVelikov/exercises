﻿namespace Army.StartUp.Contracts
{
    public interface ICommandInterpreter
    {
        void CreatePrivate(string[] input);
        void CreateSpy(string[] input);
        void CreateLeutenantGeneral(string[] input);
        void CreateEngineer(string[] input);
        void CreateCommando(string[] input);
        void FinishMission(string input);
        string Print();
    }
}