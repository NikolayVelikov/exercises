﻿using System;
using System.Linq;
using System.Collections.Generic;

using Army.Messages;
using Army.Hierarchy.Contracts;
using Army.StartUp.Contracts;

using Army.Hierarchy.Contracts.IPrivate;

namespace Army.StartUp.Core
{
    public class Repository : IRepository
    {
        private IList<ISoldier> _soldiers;
        private IList<IMission> _missions;

        public Repository()
        {
            this._soldiers = new List<ISoldier>();
            this._missions = new List<IMission>();
        }

        public IReadOnlyCollection<ISoldier> Soldiers => (IReadOnlyCollection<ISoldier>)this._soldiers;

        public void AddSoldier(ISoldier soldier)
        {
            if (soldier is null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.NullInput, "Repository", "AddSoldier"));
            }

            this._soldiers.Add(soldier);
        }

        public List<IPrivate> ReturnPrivates(IList<int> privates)
        {
            List<IPrivate> lookedPrivates = new List<IPrivate>();

            foreach (int id in privates)
            {
                IPrivate @private = this._soldiers.FirstOrDefault(p => p.Id == id) as IPrivate;

                if (@private != null)
                {
                    lookedPrivates.Add(@private);
                }
            }

            return lookedPrivates;
        }

        public IMission FindMission(string missionName)
        {
            IMission mission = this._missions.FirstOrDefault(m => m.CodeName == missionName);

            return mission;
        }

        public void AddMission(IMission mission)
        {
            if (mission != null)
            {
                this._missions.Add(mission);
            }
        }
    }
}