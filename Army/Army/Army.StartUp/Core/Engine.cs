﻿using System.Linq;

using Army.StartUp.Contracts;
using Army.StartUp.IO.IOContracts;

namespace Army.StartUp.Core
{
    public class Engine : IEngine
    {
        private ICommandInterpreter _command;
        private IReader _reader;
        private IWrite _write;
        public Engine(ICommandInterpreter command, IReader reader, IWrite write)
        {
            this._command = command;
            this._reader = reader;
            this._write = write;
        }
        public void Run()
        {
            string input = string.Empty;
            while ((input = this._reader.ReadLine()) != "End")
            {
                string[] token = input.Split(' ', System.StringSplitOptions.RemoveEmptyEntries).ToArray();
                string commandInput = token[0];
                token = token.Skip(1).ToArray();

                try
                {
                    switch (commandInput)
                    {
                        case "Private": this._command.CreatePrivate(token); break;
                        case "Spy": this._command.CreateSpy(token); break;
                        case "LieutenantGeneral": this._command.CreateLeutenantGeneral(token); break;
                        case "Engineer": this._command.CreateEngineer(token); break;
                        case "Commando": this._command.CreateCommando(token); break;
                        case "Mission": this._command.FinishMission(token[0]); break;
                    }
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    continue;
                }
            }

            this._write.WriteLine(this._command.Print());
        }
    }
}