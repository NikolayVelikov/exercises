﻿using System;
using System.Text;
using System.Collections.Generic;

using Army.Enumerators;
using Army.Hierarchy.Models;
using Army.StartUp.Contracts;
using Army.Hierarchy.Contracts;
using Army.Hierarchy.Models.Private;
using Army.Hierarchy.Contracts.IPrivate;
using Army.Hierarchy.Models.Private.LieutenantGeneral;
using Army.Hierarchy.Models.Private.SpecialisedSoldier;
using Army.Hierarchy.Contracts.IPrivate.ILieutenantGeneral;
using Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier;

namespace Army.StartUp.Core
{
    public class Service : IService
    {
        private IRepository _repo;
        public Service(IRepository repository)
        {
            this._repo = repository;
        }

        public void CreatePrivate(int id, string firstName, string lastName, decimal salary)
        {
            IPrivate @private = new Private(id, firstName, lastName, salary);

            this._repo.AddSoldier(@private);
        }

        public void CreateSpy(int id, string firstName, string lastName, int codeNumber)
        {
            ISpy spy = new Spy(id, firstName, lastName, codeNumber);

            this._repo.AddSoldier(spy);
        }

        public void CreateLeutenantGeneral(int id, string firstName, string lastName, decimal salary, List<int> privateIds)
        {
            List<IPrivate> privates = this._repo.ReturnPrivates(privateIds);

            ILieutenantGeneral general = new LieutenantGeneral(id, firstName, lastName, salary);
            foreach (IPrivate @private in privates)
            {
                general.AddPrivate(@private);
            }

            this._repo.AddSoldier(general);
        }

        public void CreateEngineer(int id, string firstName, string lastName, decimal salary, Corp corp, List<string> repairInfo)
        {
            List<IRepair> repairs = CreateRepair(repairInfo);

            IEngineer engineer = new Engineer(id, firstName, lastName, salary, corp);

            foreach (IRepair repair in repairs)
            {
                engineer.AddRepair(repair);
            }

            this._repo.AddSoldier(engineer);
        }
        private List<IRepair> CreateRepair(List<string> repairInfo)
        {
            List<IRepair> repairs = new List<IRepair>();

            for (int i = 0; i < repairInfo.Count; i += 2)
            {
                string partName = repairInfo[i];
                int workedHours = int.Parse(repairInfo[i + 1]);
                IRepair repair = new Repair(partName, workedHours);

                repairs.Add(repair);
            }

            return repairs;
        }

        public void CreateCommando(int id, string firstName, string lastName, decimal salary, Corp corp, List<string> missionInfo)
        {
            ICommando commando = new Commando(id, firstName, lastName, salary, corp);

            for (int i = 0; i < missionInfo.Count; i += 2)
            {
                string missionName = missionInfo[i];
                try
                {
                    if (!Enum.TryParse(missionInfo[i + 1], out State state))
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    IMission mission = CreateMission(missionName, state);
                    commando.AddMission(mission);
                    this._repo.AddMission(mission);
                }
                catch (ArgumentOutOfRangeException)
                {
                    continue;
                }
            }

            this._repo.AddSoldier(commando);
        }
        private IMission CreateMission(string name, State state)
        {
            IMission mission = new Mission(name, state);

            return mission;
        }

        public void FinishMission(string missionName)
        {
            IMission mission = this._repo.FindMission(missionName);

            if (mission != null)
            {
                mission.CompleteMission();
            }
        }

        public string Print()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in this._repo.Soldiers)
            {
                sb.AppendLine(item.ToString());
            }

            return sb.ToString().TrimEnd();
        }
    }
}