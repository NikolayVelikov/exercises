﻿using System;
using System.Linq;
using System.Collections.Generic;

using Army.Enumerators;
using Army.StartUp.Contracts;

namespace Army.StartUp.Core
{
    public class CommandInterpreter : ICommandInterpreter
    {
        private IService _service;

        public CommandInterpreter(IService sevice)
        {
            this._service = sevice;
        }

        public void CreatePrivate(string[] input)
        {
            int id = int.Parse(input[0]);
            string firstName = input[1];
            string lastName = input[2];
            decimal salary = decimal.Parse(input[3]);

            this._service.CreatePrivate(id, firstName, lastName, salary);
        }

        public void CreateSpy(string[] input)
        {
            int id = int.Parse(input[0]);
            string firstName = input[1];
            string lastName = input[2];
            int codeNumber = int.Parse(input[3]);

            this._service.CreateSpy(id, firstName, lastName, codeNumber);
        }

        public void CreateLeutenantGeneral(string[] input)
        {
            int id = int.Parse(input[0]);
            string firstName = input[1];
            string lastName = input[2];
            decimal salary = decimal.Parse(input[3]);
            List<int> privateIds = input.Skip(4).Select(x=> int.Parse(x)).ToList();

            this._service.CreateLeutenantGeneral(id, firstName, lastName, salary, privateIds);
        }

        public void CreateEngineer(string[] input)
        {
            Corp corp = CheckingCorp(input[4]);
            int id = int.Parse(input[0]);
            string firstName = input[1];
            string lastName = input[2];
            decimal salary = decimal.Parse(input[3]);
            List<string> repairInfo = input.Skip(5).ToList();

            this._service.CreateEngineer(id, firstName, lastName, salary, corp, repairInfo);
        }

        public void CreateCommando(string[] input)
        {
            Corp corp = CheckingCorp(input[4]);
            int id = int.Parse(input[0]);
            string firstName = input[1];
            string lastName = input[2];
            decimal salary = decimal.Parse(input[3]);
            List<string> missionInfo = input.Skip(5).ToList();

            this._service.CreateCommando(id, firstName, lastName, salary, corp, missionInfo);
        }

        private Corp CheckingCorp(string currentCorp)
        {
            if (!Enum.IsDefined(typeof(Corp), currentCorp))
            {
                throw new ArgumentOutOfRangeException();
            }

            return Enum.Parse<Corp>(currentCorp);
        }

        public void FinishMission(string input)
        {
            this._service.FinishMission(input);
        }

        public string Print()
        {
            return this._service.Print();
        }
    }
}