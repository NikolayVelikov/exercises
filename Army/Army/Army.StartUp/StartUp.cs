﻿using Army.StartUp.Core;
using Army.StartUp.Contracts;
using Army.StartUp.IO.IOModels;
using Army.StartUp.IO.IOContracts;

namespace Army.StartUp
{
    class StartUp
    {
        static void Main(string[] args)
        {
            IRepository repository = new Repository();
            IService service = new Service(repository);
            ICommandInterpreter command = new CommandInterpreter(service);

            IReader reader = new ConsoleReader();
            IWrite writer = new ConsoleWrite();

            IEngine engine = new Engine(command, reader, writer);

            engine.Run();
        }
    }
}