﻿namespace Army.StartUp.IO.IOContracts
{
    public interface IReader
    {
        string ReadLine();
    }
}