﻿using Army.StartUp.IO.IOContracts;

namespace Army.StartUp.IO.IOModels
{
    public class ConsoleWrite : IWrite
    {
        public void Write(string text)
        {
            System.Console.Write(text);
        }

        public void WriteLine(string text)
        {
            System.Console.WriteLine(text);
        }
    }
}