﻿using Army.StartUp.IO.IOContracts;

namespace Army.StartUp.IO.IOModels
{
    public class ConsoleReader : IReader
    {
        public string ReadLine()
        {
            return System.Console.ReadLine();
        }
    }
}