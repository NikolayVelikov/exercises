﻿using System;

using Army.Enumerators;
using Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier;

namespace Army.Hierarchy.Models.Private.SpecialisedSoldier
{
    public abstract class SpecialisedSoldier : Private, ISpecialisedSoldier
    {
        protected SpecialisedSoldier(int id, string firstName, string lastName, decimal salary, Corp corp)
            : base(id, firstName, lastName, salary)
        {
            this.Corp = corp;
        }

        public Corp Corp { get; }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Corps: {this.Corp.ToString()}";
        }
    }
}