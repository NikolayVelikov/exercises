﻿using System;
using System.Text;
using System.Collections.Generic;

using Army.Messages;
using Army.Enumerators;
using Army.Hierarchy.Contracts;
using Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier;

namespace Army.Hierarchy.Models.Private.SpecialisedSoldier
{
    public class Commando : SpecialisedSoldier, ICommando
    {
        private IList<IMission> _missions;
        public Commando(int id, string firstName, string lastName, decimal salary, Corp corp)
            : base(id, firstName, lastName, salary, corp)
        {
            this._missions = new List<IMission>();
        }

        public IReadOnlyCollection<IMission> Missions => (IReadOnlyCollection<IMission>)this._missions;

        public void AddMission(IMission mission)
        {
            if (mission is null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.NullInput, "Mission", "AddMission"));
            }

            this._missions.Add(mission);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine("Missions:");

            foreach (var mission in this._missions)
            {
                sb.AppendLine($"  {mission.ToString()}");
            }

            return sb.ToString().TrimEnd();
        }
    }
}