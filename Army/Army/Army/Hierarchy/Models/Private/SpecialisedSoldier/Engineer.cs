﻿using System;
using System.Text;
using System.Collections.Generic;

using Army.Messages;
using Army.Enumerators;
using Army.Hierarchy.Contracts;
using Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier;

namespace Army.Hierarchy.Models.Private.SpecialisedSoldier
{
    public class Engineer : SpecialisedSoldier, IEngineer
    {
        private IList<IRepair> _repair;
        public Engineer(int id, string firstName, string lastName, decimal salary, Corp corp) : base(id, firstName, lastName, salary, corp)
        {
            this._repair = new List<IRepair>();
        }

        public IReadOnlyCollection<IRepair> Repairs => (IReadOnlyCollection<IRepair>)this._repair;

        public void AddRepair(IRepair repair)
        {
            if (repair is null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.NullInput, "Repair", "AddRepair"));
            }

            this._repair.Add(repair);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine("Repairs:");

            foreach (var repair in this._repair)
            {
                sb.AppendLine($"  {repair.ToString()}");
            }

            return sb.ToString().TrimEnd();
        }
    }
}