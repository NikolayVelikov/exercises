﻿using System;
using System.Text;
using System.Collections.Generic;

using Army.Messages;
using Army.Hierarchy.Contracts.IPrivate;
using Army.Hierarchy.Contracts.IPrivate.ILieutenantGeneral;

namespace Army.Hierarchy.Models.Private.LieutenantGeneral
{
    public class LieutenantGeneral : Private, ILieutenantGeneral
    {
        private IList<IPrivate> _privates;

        public LieutenantGeneral(int id, string firstName, string lastName, decimal salary)
            : base(id, firstName, lastName, salary)
        {
            this._privates = new List<IPrivate>();
        }

        public IReadOnlyCollection<IPrivate> Privates => (IReadOnlyCollection<IPrivate>)this._privates;

        public void AddPrivate(IPrivate @private)
        {
            if (@private is null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.NullInput, "Private", "AddPrivate"));
            }

            this._privates.Add(@private);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine("Privates:");

            foreach (var @private in this._privates)
            {
                sb.AppendLine($"  {@private.ToString()}");
            }

            return sb.ToString().TrimEnd();
        }
    }
}