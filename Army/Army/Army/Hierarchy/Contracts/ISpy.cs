﻿namespace Army.Hierarchy.Contracts
{
    public interface ISpy : ISoldier
    {
        public int CodeNumber { get; }
    }
}