﻿using System.Collections.Generic;

namespace Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier
{
    public interface ICommando : ISpecialisedSoldier
    {
        public IReadOnlyCollection<IMission> Missions { get; }

        void AddMission(IMission mission);
    }
}