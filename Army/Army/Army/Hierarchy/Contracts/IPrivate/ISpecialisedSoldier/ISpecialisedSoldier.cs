﻿using Army.Enumerators;

namespace Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier
{
    public interface ISpecialisedSoldier : IPrivate
    {
        public Corp Corp { get; }
    }
}