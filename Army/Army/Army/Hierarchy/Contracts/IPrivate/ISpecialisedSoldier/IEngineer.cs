﻿using System.Collections.Generic;

namespace Army.Hierarchy.Contracts.IPrivate.ISpecialisedSoldier
{
    public interface IEngineer : ISpecialisedSoldier
    {
        public IReadOnlyCollection<IRepair> Repairs { get; }
        void AddRepair(IRepair repair);
    }
}