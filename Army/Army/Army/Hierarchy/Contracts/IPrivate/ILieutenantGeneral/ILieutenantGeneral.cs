﻿using System.Collections.Generic;

namespace Army.Hierarchy.Contracts.IPrivate.ILieutenantGeneral
{
    public interface ILieutenantGeneral : IPrivate
    {
        public IReadOnlyCollection<IPrivate> Privates { get; }

        void AddPrivate(IPrivate @private);
    }
}