﻿namespace Army.Hierarchy.Contracts.IPrivate
{
    public interface IPrivate : ISoldier
    {
        public decimal Salary { get; }
    }
}