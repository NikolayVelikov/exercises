﻿namespace Army.Hierarchy.Contracts
{
    public interface IRepair
    {
        public string PartName { get; }
        public int WorkedHours { get; }
    }
}
