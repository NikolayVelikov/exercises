﻿using Army.Enumerators;

namespace Army.Hierarchy.Contracts
{
    public interface IMission
    {
        public string CodeName { get; }
        public State State { get; }

        void CompleteMission();
    }
}