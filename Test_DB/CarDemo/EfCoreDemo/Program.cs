﻿using EfCoreDemo.Model;
using System;

namespace EfCoreDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new ApplicationDbContext();
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            db.Employees.Add(new Employee
            {
                FristName = "Niki",
                LastName = "Vel",
                StartWorkDate = DateTime.UtcNow,
                Salary = 1000M,
                Department = new Department { Name = "R&D"}
            });

            db.Departments.Add(new Department());
            db.SaveChanges();

        }
    }
}