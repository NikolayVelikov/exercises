﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfCoreDemo.Model
{
    public class Employee
    {
        public Employee()
        {
            this.Participations = new HashSet<EmployeeInClub>();
            this.Contracts = new HashSet<Contract>();
        }

        public int Id { get; set; }

        public string FristName { get; set; }

        public string LastName { get; set; }

        //[NotMapped]
        public string FullName => $"{this.FristName} {this.LastName}";

        public string NickName { get; set; }

        public DateTime? StartWorkDate { get; set; }

        public decimal Salary { get; set; }

        public int? BirthTownId { get; set; }
        [InverseProperty(nameof(Town.Natives))]
        public Town BirthTown { get; set; }

        public int? WorkplaceTownId { get; set; }
        [InverseProperty(nameof(Town.Workers))]
        public Town WorkplaceTown { get; set; }

        public int DepartmentId { get; set; } // Optional
        //[InverseProperty("Employees")]
        public Department Department { get; set; } // Required if i want relation

        [ForeignKey("Address")] // 1-to-1
        public int? AddressId { get; set; }
        public Address Address { get; set; }

        public ICollection<EmployeeInClub> Participations { get; set; }// with middle table
        public ICollection<Contract> Contracts { get; set; } // without middle table
    }
}