﻿using System.Collections.Generic;

namespace EfCoreDemo.Model
{
    public class Department
    {
        public Department()
        {
            this.Employees = new HashSet<Employee>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Employee> Employees { get; set; } // Optional
    }
}