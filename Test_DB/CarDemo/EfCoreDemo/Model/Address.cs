﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EfCoreDemo.Model
{
    public class Address
    {
        public int Id { get; set; }

        [ForeignKey("Employee")] // 1-to-1
        public int? EmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}