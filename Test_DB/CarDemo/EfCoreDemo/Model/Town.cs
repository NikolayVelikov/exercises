﻿using System.Collections.Generic;

namespace EfCoreDemo.Model
{
    public class Town
    {
        public Town()
        {
            this.Natives = new HashSet<Employee>();
            this.Workers = new HashSet<Employee>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Employee> Natives { get; set; }
        public ICollection<Employee> Workers { get; set; }
    }
}