﻿using System.Collections.Generic;

namespace EfCoreDemo.Model
{
    public class Contract
    {
        public Contract()
        {
            this.Employees = new HashSet<Employee>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}