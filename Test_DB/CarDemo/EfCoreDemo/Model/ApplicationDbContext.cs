﻿using EfCoreDemo.ModelBuilding;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace EfCoreDemo.Model
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {

        }

        public ApplicationDbContext(DbContextOptions option)
            : base(option)
        {

        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<EmployeeInClub> EmployeesInClubs { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Town> Towns { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Database=EfCoreDemo;Trusted_Connection=true");
            }
        }
         
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            //
            //modelBuilder.ApplyConfiguration(new DepartmentConfigurator());
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly()); // Using refelection for taking the rules

            modelBuilder.Entity<EmployeeInClub>().HasKey(x => new { x.EmployeeId, x.ClubId }); // Composite Key

            base.OnModelCreating(modelBuilder);
        }
    }
}