﻿using System.Collections.Generic;

namespace EfCoreDemo.Model
{
    public class Club
    {
        public Club()
        {
            this.Participations = new HashSet<EmployeeInClub>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<EmployeeInClub> Participations { get; set; }
    }
}