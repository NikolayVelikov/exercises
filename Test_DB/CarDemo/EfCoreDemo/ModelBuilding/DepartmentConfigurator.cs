﻿using EfCoreDemo.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfCoreDemo.ModelBuilding
{
    public class DepartmentConfigurator : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.Property(x => x.Name)
                .HasDefaultValue("Waiting").HasMaxLength(100);
        }
    }
}
