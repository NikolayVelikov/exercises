﻿using EfCoreDemo.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfCoreDemo.ModelBuilding
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            //modelBuilder.Entity<Employee>().ToTable("People", "company");//company is new name of the shema
            builder.Property(x => x.StartWorkDate).HasColumnName("StartedOn"); // change name into the table of the column

            builder.Property(x => x.FristName).HasColumnType("varchar").HasMaxLength(30).IsRequired(); // Important

            builder.Property(x => x.LastName).IsRequired().HasColumnType("varchar(30)");

            builder.Ignore(x => x.FullName); // Important

            // modelBuilder.Entity<Employee>().Property(x => x.NickName).ValueGeneratedOnAddOrUpdate(); // Will not fill. Skip filling

            builder.Property(x => x.Salary).HasPrecision(12, 3);

            builder
                .HasOne(x => x.Department)
                .WithMany(x => x.Employees)
                .HasForeignKey(x => x.DepartmentId)
                .OnDelete(DeleteBehavior.Restrict); // Default is cascade delete.
        }
    }
}
