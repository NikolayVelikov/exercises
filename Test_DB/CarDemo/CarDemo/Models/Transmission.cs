﻿using System.ComponentModel.DataAnnotations;

namespace CarDemo.Models
{
    public class Transmission
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Type { get; set; }

        public int Shifts { get; set; }
    }
}