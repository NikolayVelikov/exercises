﻿using System.ComponentModel.DataAnnotations;

namespace CarDemo.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Model { get; set; }

        public int BrandId { get; set; }
        public Brand Brand { get; set; }

        public int EngineId { get; set; }
        public Engine Engine { get; set; }

        public int TransmissionId { get; set; }
        public Transmission Transmission { get; set; }
    }
}