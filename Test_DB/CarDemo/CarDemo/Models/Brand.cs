﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDemo.Models
{
    public class Brand
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public ICollection<Car> Cars { get; set; }
    }
}
