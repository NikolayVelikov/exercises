﻿using Microsoft.EntityFrameworkCore;

namespace CarDemo.Models
{
    public class ShowRoomContext : DbContext
    {
        public ShowRoomContext()
        {

        }

        public ShowRoomContext(DbContextOptions options)
            :base(options)
        {

        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Engine> Engines { get; set; }
        public DbSet<Transmission> Transmissions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Database=ShowRoom;Integrated Security = true");
            }
            base.OnConfiguring(optionsBuilder);
        }
    }
}