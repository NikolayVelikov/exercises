﻿using System.ComponentModel.DataAnnotations;

namespace CarDemo.Models
{
    public class Engine
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(15)]
        public string Type { get; set; }
    }
}
