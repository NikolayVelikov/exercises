﻿using Easter.Core.Contracts;
using Easter.Models.Bunnies.Contracts;
using Easter.Models.Bunnies.Models;
using Easter.Models.Dyes;
using Easter.Models.Dyes.Contracts;
using Easter.Models.Eggs;
using Easter.Models.Eggs.Contracts;
using Easter.Repositories;
using Easter.Utilities.Messages;
using System;
using System.Linq;
using System.Collections.Generic;
using Easter.Models.Workshops;
using System.Text;

namespace Easter.Core
{
    public class Controller : IController
    {
        private BunnyRepository _bunnies;
        private EggRepository _eggs;

        public Controller()
        {
            this._bunnies = new BunnyRepository();
            this._eggs = new EggRepository();
        }
        public string AddBunny(string bunnyType, string bunnyName)
        {
            IBunny bunny;
            switch (bunnyType)
            {
                case nameof(HappyBunny): bunny = new HappyBunny(bunnyName); break;
                case nameof(SleepyBunny): bunny = new SleepyBunny(bunnyName); break;
                default: throw new InvalidOperationException(ExceptionMessages.InvalidBunnyType);
            }

            this._bunnies.Add(bunny);

            return string.Format(OutputMessages.BunnyAdded, bunnyType, bunnyName);
        }

        public string AddDyeToBunny(string bunnyName, int power)
        {
            IBunny bunny = this._bunnies.FindByName(bunnyName);
            if (bunny == null)
            {
                throw new InvalidOperationException(ExceptionMessages.InexistentBunny);
            }

            IDye dye = new Dye(power);
            bunny.AddDye(dye);

            return string.Format(OutputMessages.DyeAdded, power, bunnyName);
        }

        public string AddEgg(string eggName, int energyRequired)
        {
            IEgg egg = new Egg(eggName, energyRequired);

            this._eggs.Add(egg);

            return string.Format(OutputMessages.EggAdded, eggName);
        }

        public string ColorEgg(string eggName)
        {
            List<IBunny> kkk = new List<IBunny>(this._bunnies.Models);
            kkk = kkk.OrderByDescending(x => x.Energy).ToList();
            IEgg egg = this._eggs.FindByName(eggName);
            IBunny bunny;
            while (!egg.IsDone())
            {
                bunny = kkk.FirstOrDefault(b => b.Energy >= 50 && b.Dyes.Count(x => x.IsFinished() == false) != 0);
                if (this._bunnies.Models.Count == 0)
                {
                    break;
                }
                if (bunny == null)
                {
                    throw new InvalidOperationException(ExceptionMessages.BunniesNotReady);
                }

                Workshop shop = new Workshop();
                shop.Color(egg, bunny);
                if (bunny.Energy == 0)
                {
                    this._bunnies.Remove(bunny);
                }
            }



            string message = string.Empty;
            if (egg.IsDone())
            {
                message = string.Format(OutputMessages.EggIsDone, egg.Name);
            }
            else
            {
                message = string.Format(OutputMessages.EggIsNotDone, egg.Name);
            }

            return message;
        }

        public string Report()
        {
            StringBuilder sb = new StringBuilder();

            List<IEgg> colored = this._eggs.Models.Where(e => e.IsDone() == true).ToList();
            sb.AppendLine($"{colored.Count} eggs are done!");
            sb.AppendLine("Bunnies info:");

            foreach (var bunny in this._bunnies.Models)
            {
                sb.AppendLine(bunny.ToString());
            }

            return sb.ToString().TrimEnd();
        }
    }
}
