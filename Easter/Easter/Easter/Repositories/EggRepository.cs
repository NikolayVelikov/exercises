﻿namespace Easter.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Easter.Models.Eggs.Contracts;
    using Easter.Repositories.Contracts;

    public class EggRepository : IRepository<IEgg>
    {
        private IList<IEgg> _eggs;

        public EggRepository()
        {
            this._eggs = new List<IEgg>();
        }

        public IReadOnlyCollection<IEgg> Models => (IReadOnlyCollection<IEgg>)this._eggs;

        public void Add(IEgg model)
        {
            if (!this._eggs.Any(e => e.Name == model.Name))
            {
                this._eggs.Add(model);
            }
        }

        public IEgg FindByName(string name)
        {
            return this._eggs.FirstOrDefault(e => e.Name == name);
        }

        public bool Remove(IEgg model)
        {
            IEgg deletion = this._eggs.FirstOrDefault(e => e.Name == model.Name);

            return this._eggs.Remove(deletion);
        }
    }
}
