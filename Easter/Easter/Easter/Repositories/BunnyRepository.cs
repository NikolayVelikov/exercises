﻿namespace Easter.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Easter.Models.Bunnies.Contracts;
    using Easter.Repositories.Contracts;

    public class BunnyRepository : IRepository<IBunny>
    {
        private IList<IBunny> _bunnies;

        public BunnyRepository()
        {
            this._bunnies = new List<IBunny>();
        }

        public IReadOnlyCollection<IBunny> Models => (IReadOnlyCollection<IBunny>)this._bunnies;

        public void Add(IBunny model)
        {            
            if (!this._bunnies.Any(b => b.Name == model.Name))
            {
                this._bunnies.Add(model);
            }
        }

        public IBunny FindByName(string name)
        {
            return this._bunnies.FirstOrDefault(b => b.Name == name);
        }

        public bool Remove(IBunny model)
        {
            IBunny deletion = this._bunnies.FirstOrDefault(b => b.Name == model.Name);

            return this._bunnies.Remove(deletion);
        }
    }
}
