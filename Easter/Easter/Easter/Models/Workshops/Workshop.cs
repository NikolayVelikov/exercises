﻿namespace Easter.Models.Workshops
{
    using Easter.Models.Eggs.Contracts;
    using Easter.Models.Bunnies.Contracts;
    using Easter.Models.Workshops.Contracts;

    public class Workshop : IWorkshop
    {
        public void Color(IEgg egg, IBunny bunny)
        {
            foreach (var dye in bunny.Dyes)
            {
                while (!dye.IsFinished() && bunny.Energy > 0 && !egg.IsDone())
                {
                    bunny.Work();
                    dye.Use();
                    egg.GetColored();
                }
            }
        }
    }
}