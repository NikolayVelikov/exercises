﻿namespace Easter.Models.Dyes
{
    using Easter.Models.Dyes.Contracts;

    public class Dye : IDye
    {
        private int _power;
        private const int decresingPower = 10;

        public Dye(int power)
        {
            this.Power = power;
        }

        public int Power
        {
            get => this._power;
            private set
            {
                if (value < 0)
                {
                    this._power = 0;
                }
                else
                {
                    this._power = value;
                }
            }
        }

        public bool IsFinished()
        {
            if (this.Power == 0)
            {
                return true;
            }

            return false;
        }

        public void Use()
        {
            this.Power -= decresingPower;
        }
    }
}