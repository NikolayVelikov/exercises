﻿namespace Easter.Models.Eggs
{
    using System;

    using Easter.Utilities.Messages;
    using Easter.Models.Eggs.Contracts;

    public class Egg : IEgg
    {
        private string _name;
        private int _energyRequired;
        private const int decreaseEnergy = 10;

        public Egg(string name, int energyRequired)
        {
            this.Name = name;
            this.EnergyRequired = energyRequired;
        }

        public string Name
        {
            get => this._name;
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ExceptionMessages.InvalidEggName);
                }

                this._name = value;
            }
        }

        public int EnergyRequired
        {
            get => this._energyRequired;
            private set
            {
                if (value < 0)
                {
                    this._energyRequired = 0;
                }
                else
                {
                    this._energyRequired = value;
                }
            }
        }

        public void GetColored()
        {
            this.EnergyRequired -= decreaseEnergy;
        }

        public bool IsDone()
        {
            if (this.EnergyRequired == 0)
            {
                return true;
            }

            return false;
        }
    }
}
