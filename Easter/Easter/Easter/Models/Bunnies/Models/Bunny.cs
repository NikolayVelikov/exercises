﻿namespace Easter.Models.Bunnies.Models
{
    using System;
    using System.Collections.Generic;

    using Easter.Utilities.Messages;
    using Easter.Models.Dyes.Contracts;
    using Easter.Models.Bunnies.Contracts;
    using System.Text;
    using System.Linq;

    public abstract class Bunny : IBunny
    {
        private string _name;
        private int _energy;
        private IList<IDye> _dyes;

        public const int decreasingEnergy = 10;

        private Bunny()
        {
            this._dyes = new List<IDye>();
        }
        protected Bunny(string name, int energy)
            : this()
        {
            this.Name = name;
            this.Energy = energy;
        }
        public string Name
        {
            get => this._name;
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ExceptionMessages.InvalidBunnyName);
                }

                this._name = value;
            }
        }

        public int Energy
        {
            get => this._energy;
            protected set
            {
                if (value < 0)
                {
                    this._energy = 0;
                }
                else
                {
                    this._energy = value;
                }
            }
        }

        public ICollection<IDye> Dyes => this._dyes;

        public void AddDye(IDye dye)
        {
            this._dyes.Add(dye);
        }

        public virtual void Work()
        {
            this.Energy -= decreasingEnergy;
        }

        public override string ToString()
        {
            int count = this.Dyes.Where(x => x.IsFinished() != true).Count();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Name: {this.Name}");
            sb.AppendLine($"Energy: {this.Energy}");
            sb.AppendLine($"Dyes: {count} not finished");

            return sb.ToString().TrimEnd();
        }
    }
}