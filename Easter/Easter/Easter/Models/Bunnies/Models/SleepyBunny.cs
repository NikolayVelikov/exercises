﻿namespace Easter.Models.Bunnies.Models
{
    public class SleepyBunny : Bunny
    {
        private const int initialEnergy = 50;
        private new const int decreasingEnergy = 15;
        public SleepyBunny(string name) 
            : base(name, initialEnergy)
        {
        }

        public override void Work()
        {
            base.Energy -= decreasingEnergy;
        }
    }
}
