﻿using System;
using System.Collections.Generic;

namespace Reverse_String
{
    public class Program
    {
        public static void Main()
        {
            string str = "Reverse me!!";
            string reverse = Reverse(str);

            Console.WriteLine("Answer: {0}", reverse);
        }

        private static string Reverse(string str)
        {
            Stack<char> stack = new Stack<char>();
            foreach (var letter in str)
            {
                stack.Push(letter);
            }

            return string.Join("", stack);
        }
    }
}