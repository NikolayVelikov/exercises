﻿using System;
using System.Linq;

namespace Linq
{

    public class Program
    {
        static readonly string[] Fruits = new[]
        {
        "Acai",
        "Apple",
        "Apricots",
        "Banana",
        "Blackberry",
        "",
        "Blueberry",
        "Cherries",
        "Coconut",
        "Cranberry ",
        "Cucumber",
        "Currents",
        "Dates",
        "Durian",
        "Fig",
        "Goji berries",
        "Gooseberry",
        "Grapefruit",
        "Grapes",
        "Jackfruit",
        "Kiwi",
        "Kumquat",
        "Lemon",
        "Lime",
        "Lucuma",
        " Lychee",
        "Mango",
        "Mangosteen",
        "Melon",
        "",
        "Mulberry",
        "Nectarine",
        "Orange",
        "Papaya",
        "Passion Fruit",
        "Peach",
        "Pear",
        "Pineapple ",
        "Plum",
        "Pomegranate",
        "Pomelo",
        "Prickly Pear",
        "Prunes",
        "Raspberries",
        "Strawberries",
        "Tangerine",
        "Watermelon"
    };

        public static void Main()
        {
            string[] query = Fruits.Where(f =>
                                        !string.IsNullOrWhiteSpace(f)
                                        && f.Split(' ', StringSplitOptions.RemoveEmptyEntries).Length == 1)
                                   .Select(f => f.TrimStart().TrimEnd().ToLower())
                                   .Select(f => f.TrimStart().TrimEnd().ToLower())
                                   .OrderByDescending(f => f.Length)
                                   .ToArray();

            Console.WriteLine("Answer: {0}", String.Join(", ", query));
        }
    }
}