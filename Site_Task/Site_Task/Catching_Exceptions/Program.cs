﻿using System;

namespace Catching_Exceptions
{
    public class Program
    {
        public static void Main()
        {
            Result result = null;
            try
            {
                result = GetResult();
                Console.WriteLine(result.Successful ? "It worked!!" : "Oops, something went wrong...");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Oops, something went wrong...");
            }
        }

        private static Result GetResult()
        {
            Result result = new Result();

            ExternalDependency service = new ExternalDependency();
            service.DoWork();

            result.Successful = true;
            return result;
        }
    }

    public class Result
    {
        public bool Successful { get; set; }
    }

    // Please do not change the following class.
    // Assume that it is an external dependency that is out of your control.
    sealed class ExternalDependency
    {
        public void DoWork()
        {
            int[] array = new int[1];
            array[new Random().Next(2)] = 0;
        }
    }
}