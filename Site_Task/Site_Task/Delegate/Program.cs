﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Delegate
{

    public class Program
    {
        public static void Main()
        {
            var clock = new ClockTower(1000);
            // Keep the main thread alive long enough to observe the event.
            Thread.Sleep(3000);
        }
    }

    public sealed class ClockTower
    {
        private readonly Stopwatch _stopWatch = new Stopwatch();
        private readonly int _delayMilliseconds;
        private readonly Service _service = new Service();

        public ClockTower(int delayMilliseconds)
        {
            this.OnChime += this._service.OnPrepared;
            _stopWatch.Start();
            _delayMilliseconds = delayMilliseconds;
            DelayedChime();
            
        }

        public delegate void ChimeEventHandler(object sender, ChimeEventArgs e);
        public event ChimeEventHandler OnChime;
        public async void DelayedChime()
        {
            await Task.Delay(_delayMilliseconds);
            OnPrepared();
            // OnChime(this, new ChimeEventArgs(_stopWatch.ElapsedMilliseconds));

            await Task.Delay(_delayMilliseconds);
            OnPrepared();
            //OnChime(this, new ChimeEventArgs(_stopWatch.ElapsedMilliseconds));
        }

        protected void OnPrepared()
        {
            OnChime?.Invoke(this, new ChimeEventArgs(_stopWatch.ElapsedMilliseconds));
        }
    }

    public class Service
    {
        public void OnPrepared(object source, ChimeEventArgs arg)
        {
            Console.WriteLine(arg.Message);
        }
    }

    public class ChimeEventArgs
    {
        public ChimeEventArgs(long milliseconds)
        {
            Message = String.Format("The clock chimed after {0} seconds", milliseconds / 1000M);
        }

        public string Message { get; set; }
    }
}
