﻿using System;

namespace Generic
{
    public class Program
    {
        public static void Main()
        {
            ResponseString responseString = new ResponseString { Success = true, StatusCode = 200, Data = "{ foo: \"bar\" }" };
            ResponseDecimal responseDecimal = new ResponseDecimal { Success = true, StatusCode = 200, Data = 19.99M };
            ResponseDateTime responseDateTime = new ResponseDateTime { Success = true, StatusCode = 200, Data = DateTime.Parse("4/13/2015 4:00PM") };

            Console.WriteLine("String Response: {0}", responseString.Data);
            Console.WriteLine("Decimal Response: {0}", responseDecimal.Data);
            Console.WriteLine("DateTime Response: {0}", responseDateTime.Data);
        }
    }

    public interface IResponce<T>
    {
        bool Success { get; set; }
        int StatusCode { get; set; }
        T Data { get; set; }
    }

    public abstract class Responce<T> : IResponce<T>
    {
        public bool Success {get; set;}
        public int StatusCode {get; set;}
        public T Data {get; set;}
    }

    public class ResponseString : Responce<string>
    {
    }

    public class ResponseDecimal : Responce<decimal?>
    {
    }

    public class ResponseDateTime : Responce<DateTime>
    {
    }
}