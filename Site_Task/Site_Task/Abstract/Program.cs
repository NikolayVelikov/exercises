﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Abstract
{
    public class Program
    {
        public static void Main()
        {
            Robot robot = new Sonny();

            Console.WriteLine("Greeting: {0}", robot.Greeting());
            Console.WriteLine("Robot Version: {0}", robot.Version);

            List<string> laws = robot.GetLaws();
            foreach (string law in laws)
            {
                Console.WriteLine("Law #{0}: {1}", laws.IndexOf(law) + 1, law);
            }
        }
    }

    public class Sonny : Robot
    {
        public Sonny()
            : base(new Version("2.0.0.0"))
        {
        }

        public override string Greeting()
        {
            return $"Hello, my name is {this.GetType().Name}";
        }

        public override List<string> GetLaws()
        {
            IList<string> baseLaws = base.GetLaws();
            baseLaws.Add("A robot may not harm humanity, or, by inaction, allow humanity to come to harm");

            return baseLaws.ToList();
        }
    }


    public abstract class Robot
    {
        private readonly List<string> _laws = new List<string>
    {
        "A robot may not injure a human being or, through inaction, allow a human being to come to harm.",
        "A robot must obey the orders given it by human beings, except where such orders would conflict with the First Law.",
        "A robot must protect its own existence as long as such protection does not conflict with the First or Second Law."
    };

        protected Robot(Version version)
        {
            Version = version;
        }

        public abstract string Greeting();

        public Version Version { get; set; }

        public virtual List<string> GetLaws()
        {
            return _laws;
        }
    }
}
