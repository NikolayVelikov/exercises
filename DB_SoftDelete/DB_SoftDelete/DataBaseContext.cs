﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DB_SoftDelete
{
    public class DataBaseContext:DbContext
    {
        public DataBaseContext()
        {

        }

        public DbSet<Person> People { get; set; }
        public DbSet<Car> Cars { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().HasMany(x => x.Cars).WithOne(x=> x.Person).HasForeignKey(x => x.PersonId).OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Car>().HasIndex(x => x.Brand).IsUnique();


            modelBuilder.Entity<Car>().HasQueryFilter(c => !c.IsDeleted);
            modelBuilder.Entity<Person>().HasQueryFilter(p => !p.IsDeleted);

            var persons = new List<Person>()
            {
                new Person{Id =1,Name="DemoPerson1"},
                new Person{Id =2,Name="DemoPerson2"},
                new Person{Id =3,Name="DemoPerson3"},
            };
            var cars = new List<Car>()
            {
                new Car{Id = 1,Brand ="Car1",PersonId=1},
                new Car{Id = 2,Brand ="Car2",PersonId=1},
                new Car{Id = 3,Brand ="Car3",PersonId=1},
                new Car{Id = 4,Brand ="Car4",PersonId=2},
                new Car{Id = 5,Brand ="Car5",PersonId=3},
            };

            modelBuilder.Entity<Person>().HasData(persons);
            modelBuilder.Entity<Car>().HasData(cars);
            
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Database=SoftDeleteDemo;Trusted_Connection=true");
            }

            base.OnConfiguring(optionsBuilder);
        }

        
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }
        
    }
}
