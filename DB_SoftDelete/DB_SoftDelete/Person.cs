﻿using System.Collections.Generic;

namespace DB_SoftDelete
{
    public class Person:Entity
    {
        public string Name { get; set; }

        public ICollection<Car> Cars { get; set; }
    }
}
