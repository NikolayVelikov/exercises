﻿namespace DB_SoftDelete
{
    public class Car : Entity
    {
        public string Brand { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}