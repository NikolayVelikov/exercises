﻿using System;
using System.Collections.Generic;

namespace CheckBrackets
{
    public class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            Stack<char> brakets = new Stack<char>();

            foreach (var bracket in input)
            {
                bool isOpeningBracket = bracket == 40 || bracket == 91 || bracket == 123;
                bool isClosingBracket = bracket == 41 || bracket == 93 || bracket == 125;

                if (!isOpeningBracket && !isClosingBracket)
                {
                    continue;
                }

                if (brakets.Count == 0)
                {
                    brakets.Push(bracket);

                    if (isClosingBracket)
                    {
                        break;
                    }
                }
                else if (isOpeningBracket)
                {
                    brakets.Push(bracket);
                }
                else if (isClosingBracket)
                {
                    char previous = brakets.Peek();
                    if (previous == 40 && bracket == 41)
                    {
                        continue;
                    }
                    else if (previous == 91 && bracket == 93)
                    {
                        continue;
                    }
                    else if (previous == 123 && bracket == 125)
                    {
                        continue;
                    }

                    brakets.Push(previous);
                    break;
                }
            }

            Console.WriteLine(brakets.Count == 0);
        }
    }
}