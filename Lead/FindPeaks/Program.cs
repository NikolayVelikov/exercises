﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace FindPeaks
{
    public class Program
    {
        static void Main(string[] args)
        {
            char[] bounders = new char[] { '[', ']', ' ', ',' };
            int[] peaks = Console.ReadLine()
                                 .Split(bounders, StringSplitOptions.RemoveEmptyEntries)
                                 .Select(x => int.Parse(x))
                                 .ToArray();

            List<string> output = new List<string>();
            for (int i = 0; i < peaks.Length; i++)
            {
                int left = 0;
                if (i >= 1)
                {
                    left = peaks[i - 1];
                }

                int current = peaks[i];

                int right = 0;
                if (i <= peaks.Length - 2)
                {
                    right = peaks[i + 1];
                }

                if (left < current && current > right)
                {
                    string result = $"{i}, {current}";
                    output.Add(result);
                    i++;
                }
            }

            Console.WriteLine(string.Join("; ", output));
        }
    }
}