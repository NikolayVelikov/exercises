﻿namespace Weapon_Shop.Constraints
{
    public class Parameters
    {
        public const int InitialHealth = 100;
        public const int InitialMana = 100;
        public const int InitialMoney = 700;

        public const int WarriorInitialStrenght = 30;
        public const int WizzardInitialStrenght = 15;
        public const int RogueInitialStrenght = 0;

        public const int WarriorInitialAbilityPower = 10;
        public const int WizzardInitialAbilityPower = 25;
        public const int RogueInitialAbilityPower = 5;

        public const int WarriorInitialAgility = 5;
        public const int WizzardInitialAgility = 10;
        public const int RogueInitialAgility = 30;

        public const int SwordHP = 10;
        public const int SwordMP = 5;
        public const int SwordStr = 14;
        public const int SwordAP = 0;
        public const int SwordAG = 3;
        public const decimal SwordPrice = 50;

        public const int GreatswordHP = 15;
        public const int GreatswordMP = 0;
        public const int GreatswordStr = 20;
        public const int GreatswordAP = 0;
        public const int GreatswordAG = -3;
        public const decimal GreatswordPrice = 70;

        public const int RapierHP = 5;
        public const int RapierMP = 7;
        public const int RapierStr = 10;
        public const int RapierAP = 0;
        public const int RapierAG = 7;
        public const decimal RapierPrice = 30;
                
    }
}
