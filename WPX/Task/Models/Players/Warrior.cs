﻿using Weapon_Shop.Constraints;

namespace Weapon_Shop.Models.Players
{
    public class Warrior : Player
    {
        public Warrior(string name,decimal money) 
            : base(name, 
                  Parameters.InitialHealth,
                  Parameters.InitialMana,
                  Parameters.WarriorInitialStrenght,
                  Parameters.WarriorInitialAbilityPower,
                  Parameters.WarriorInitialAgility, 
                  money)
        {
        }
    }
}