﻿using Weapon_Shop.Constraints;

namespace Weapon_Shop.Models.Players
{
    public class Rogue : Player
    {
        public Rogue(string name, decimal money)
            : base(name,
                  Parameters.InitialHealth,
                  Parameters.InitialMana,
                  Parameters.RogueInitialStrenght,
                  Parameters.RogueInitialAbilityPower,
                  Parameters.RogueInitialAgility,
                  money)
        {
        }
    }
}