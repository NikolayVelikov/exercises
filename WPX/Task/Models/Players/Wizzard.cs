﻿using Weapon_Shop.Constraints;

namespace Weapon_Shop.Models.Players
{
    public class Wizzard : Player
    {
        public Wizzard(string name, decimal money)
            : base(name,
                  Parameters.InitialHealth,
                  Parameters.InitialMana,
                  Parameters.WizzardInitialStrenght,
                  Parameters.WizzardInitialAbilityPower,
                  Parameters.WizzardInitialAgility,
                  money)
        {
        }
    }
}