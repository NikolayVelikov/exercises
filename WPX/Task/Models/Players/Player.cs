﻿using System;

using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Models.Players
{
    public abstract class Player : Entity, IPlayer
    {
        private IWeapon weapon;

        protected Player(string name, int health, int mana, int streght, int abilityPower, int agility, decimal money)
            : base(name, health, mana, streght, abilityPower, agility)
        {
            this.IsDead = false;
            this.Money = money;
        }
        public decimal Money { get; private set; }

        public bool IsDead { get; private set; }

        public IWeapon Weapons => weapon;

        public void AddWeapon(IWeapon weapon)
        {
            if (weapon.Price > this.Money)
            {
                throw new Exception($"Cannot buy this weapon!!!. You have Money:{this.Money}");
            }

            weapon.DecreaseMonyAfterBuying();
            this.Money -= weapon.Price;
            this.weapon = weapon;

            this.Health += weapon.Health;
            this.Mana += weapon.Mana;
            this.Strength += weapon.Strength;
            this.AbilityPower += weapon.AbilityPower;
            this.Agility += weapon.Agility;
        }

        public override string ToString()
        {            
            return $"Name:{this.Name}, Type:{this.GetType().Name}, Weapon:{this.weapon.ToString()}";
        }
    }
}