﻿using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Models
{
    public abstract class Entity : IEntity
    {
        protected Entity(string name, int health, int mana, int streght, int abilityPower, int agility)
        {
            this.Name = name;
            this.Health = health;
            this.Mana = mana;
            this.Strength = streght;
            this.AbilityPower = abilityPower;
            this.Agility = agility;
        }
        public string Name { get; protected set; }

        public int Health { get; protected set; }

        public int Mana { get; protected set; }

        public int Strength { get; protected set; }

        public int AbilityPower { get; protected set; }

        public int Agility { get; protected set; }
    }
}