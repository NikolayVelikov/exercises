﻿using System;
using System.Linq;
using System.Collections.Generic;

using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Models
{
    public class Shop : IShop
    {
        private readonly ICollection<IWeapon> weapons;

        public Shop()
        {
            this.weapons = new List<IWeapon>();
        }

        public IReadOnlyCollection<IWeapon> Weapons => (IReadOnlyCollection<IWeapon>)this.weapons;

        public string AddWeaponInStore(IWeapon weapon)
        {
            if (weapon == null)
            {
                throw new Exception($"Weapon is not created");
            }

            IWeapon storeWeapon = this.weapons.FirstOrDefault(w => w.Name.ToLower() == weapon.Name.ToLower());
            if (storeWeapon != null)
            {
                throw new Exception($"Weapon with Name:{weapon.Name} already exist");
            }

            this.weapons.Add(weapon);
            return $"Weapong with Name: {weapon.Name} is for selling with Price: {weapon.Price}";
        }

        public IEnumerable<IWeapon> Catalogue()
        {
            return this.weapons;
        }

        public void SellWeapon(IWeapon wantedWeapon)
        {
            this.weapons.Remove(wantedWeapon);
        }
    }
}
