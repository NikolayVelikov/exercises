﻿namespace Weapon_Shop.Models.Contracts
{
    public interface IPlayer : IEntity
    {
        decimal Money { get; }

        bool IsDead { get; }

        IWeapon Weapons { get; }

        void AddWeapon(IWeapon weapon);
    }
}