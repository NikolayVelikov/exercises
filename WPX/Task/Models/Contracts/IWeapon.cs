﻿namespace Weapon_Shop.Models.Contracts
{
    public interface IWeapon : IEntity
    {
        decimal Price { get; }

        void DecreaseMonyAfterBuying();
    }
}