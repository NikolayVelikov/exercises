﻿namespace Weapon_Shop.Models.Contracts
{
    public interface IEntity
    {
        string Name { get; }

        int Health { get; }

        int Mana { get; }

        int Strength { get; }

        int AbilityPower { get; }

        int Agility { get; }
    }
}