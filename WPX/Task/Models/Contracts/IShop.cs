﻿using System.Collections.Generic;

namespace Weapon_Shop.Models.Contracts
{
    public interface IShop
    {
        IReadOnlyCollection<IWeapon> Weapons { get; }

        void SellWeapon(IWeapon wantedWeapon);

        IEnumerable<IWeapon> Catalogue();

        string AddWeaponInStore(IWeapon weapon);
    }
}