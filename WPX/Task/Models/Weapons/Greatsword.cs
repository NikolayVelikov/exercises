﻿using Weapon_Shop.Constraints;

namespace Weapon_Shop.Models.Weapons
{
    public class Greatsword : Weapon
    {
        public Greatsword(string name)
            : base(name,
                  Parameters.GreatswordHP,
                  Parameters.GreatswordMP,
                  Parameters.GreatswordStr,
                  Parameters.GreatswordAP,
                  Parameters.GreatswordAG,
                  Parameters.GreatswordPrice)
        {
        }
    }
}