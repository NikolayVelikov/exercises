﻿using System.Text;

using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Models.Weapons
{
    public abstract class Weapon : Entity, IWeapon
    {
        protected Weapon(string name, int health, int mana, int strength, int abilityPower, int agility, decimal price)
            : base(name, health, mana, strength, abilityPower, agility)
        {
            this.Price = price;
        }

        public decimal Price { get; private set; }

        public void DecreaseMonyAfterBuying()
        {
            this.Price = this.Price / 2;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Type:{this.GetType().Name}");
            sb.AppendLine($"Name:{this.Name}");
            sb.AppendLine($"Price:{this.Price}");
            sb.AppendLine($"Health:{this.Health}, Mana:{this.Mana}, Strength:{this.Strength}, Ability Power:{this.AbilityPower}, Agility:{this.Agility}");

            return sb.ToString().TrimEnd();
        }
    }
}