﻿using Weapon_Shop.Constraints;

namespace Weapon_Shop.Models.Weapons
{
    public class Sword : Weapon
    {
        public Sword(string name) 
            : base(name,
                  Parameters.SwordHP,
                  Parameters.SwordMP,
                  Parameters.SwordStr,
                  Parameters.SwordAP,
                  Parameters.SwordAG,
                  Parameters.SwordPrice)
        {
        }
    }
}