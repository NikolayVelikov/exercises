﻿using Weapon_Shop.Constraints;

namespace Weapon_Shop.Models.Weapons
{
    class Rapier : Weapon
    {
        public Rapier(string name)
            : base(name,
                  Parameters.RapierHP,
                  Parameters.RapierMP,
                  Parameters.RapierStr,
                  Parameters.RapierAP,
                  Parameters.RapierAG,
                  Parameters.RapierPrice)
        {
        }
    }
}