﻿namespace Weapon_Shop.Core.Contracts
{
    public interface IWriter
    {
        void Write(string text);

        void WriteLine(string text);

        void ClearConsole();
    }
}