﻿namespace Weapon_Shop.Core.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}