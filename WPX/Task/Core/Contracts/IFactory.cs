﻿using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Core.Contracts
{
    public interface IFactory
    {
        IShop CreateShop();

        IWeapon CreateWeapon(string name, string type);

        IPlayer CreatePlayer(string name, string type);
    }
}