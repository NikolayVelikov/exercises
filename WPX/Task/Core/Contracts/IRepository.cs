﻿using System.Collections.Generic;

using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Core.Contracts
{
    public interface IRepository
    {
        IReadOnlyCollection<IPlayer> Players { get; }

        IShop Shop { get; }

        void AddPlayer(IPlayer player);

        IPlayer TakePlayer(string name);

        void AddShop(IShop shop);
    }
}