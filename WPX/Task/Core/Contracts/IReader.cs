﻿namespace Weapon_Shop.Core.Contracts
{
    public interface IReader
    {
        string ReadLine();
    }
}