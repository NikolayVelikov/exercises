﻿using System;
using System.Linq;
using System.Collections.Generic;

using Weapon_Shop.Core.Contracts;
using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Core
{
    public class Repository : IRepository
    {
        private readonly IList<IPlayer> players;
        private IShop shop;
        
        public Repository()
        {
            this.players = new List<IPlayer>();
            this.shop = null;
        }

        public IReadOnlyCollection<IPlayer> Players => (IReadOnlyCollection<IPlayer>)this.players;

        public IShop Shop => this.shop;

        public void AddPlayer(IPlayer player)
        {
            if (player == null)
            {
                throw new Exception($"Player is not created");
            }
            bool isExist = this.players.Any(p => p.Name.ToLower() == player.Name.ToLower());
            if (isExist)
            {
                throw new Exception($"Player with such name already exist!!!");
            }

            this.players.Add(player);
        }

        public IPlayer TakePlayer(string name)
        {
            IPlayer player = this.players.FirstOrDefault(p => p.Name.ToLower() == name.ToLower());

            return player;
        }

        public void AddShop(IShop shop)
        {
            if (this.shop != null)
            {
                throw new Exception($"Shop already exist!!!");
            }

            this.shop = shop;
        }
    }
}
