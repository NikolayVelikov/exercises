﻿using System;

using Weapon_Shop.Core.Contracts;

namespace Weapon_Shop.Core
{
    public class ConsoleReader : IReader
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}