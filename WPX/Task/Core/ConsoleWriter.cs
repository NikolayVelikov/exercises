﻿using System;

using Weapon_Shop.Core.Contracts;

namespace Weapon_Shop.Core
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string text)
        {
            Console.Write(text);
        }

        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }

        public void ClearConsole()
        {
            Console.Clear();
        }
    }
}