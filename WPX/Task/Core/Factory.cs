﻿using Weapon_Shop.Models;
using Weapon_Shop.Core.Contracts;
using Weapon_Shop.Models.Players;
using Weapon_Shop.Models.Weapons;
using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Core
{
    public class Factory : IFactory
    {
        private readonly decimal initialMoney;

        public Factory(decimal money)
        {
            this.initialMoney = money;
        }

        public IPlayer CreatePlayer(string name, string type)
        {            
            IPlayer player = null;
            switch (type.ToLower())
            {
                case "warrior": player = new Warrior(name, this.initialMoney); break;
                case "wizzard": player = new Wizzard(name, this.initialMoney); break;
                case "rogue": player = new Rogue(name, this.initialMoney); break;
            }

            return player;
        }

        public IShop CreateShop()
        {
            return new Shop();
        }

        public IWeapon CreateWeapon(string name, string type)
        {
            IWeapon weapon = null;
            switch (type.ToLower())
            {
                case "sword": weapon = new Sword(name); break;
                case "greatsword": weapon = new Greatsword(name); break;
                case "rapier": weapon = new Rapier(name); break;
            }

            return weapon;
        }
    }
}