﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Weapon_Shop.Core.Contracts;
using Weapon_Shop.Models.Contracts;

namespace Weapon_Shop.Core
{
    public class Engine : IEngine
    {
        private readonly IFactory factory;
        private readonly IRepository repo;
        private readonly IWriter writer;
        private readonly IReader reader;

        public Engine(IFactory factory, IRepository repo, IWriter writer, IReader reader)
        {
            this.factory = factory;
            this.repo = repo;
            this.writer = writer;
            this.reader = reader;
        }

        public void Run()
        {
            this.RegisterPlayer();
            this.RegisterShopAndInitialWeapons(); // Creating the store and wepons inside

            bool isPlaying = true;
            while (isPlaying)
            {
                this.writer.WriteLine(null);
                this.writer.WriteLine("Select one of the statment and follow the rules:");
                this.writer.WriteLine("1# Buy Weapon.");
                this.writer.WriteLine("2# See Character Stats.");
                this.writer.WriteLine("3# Press any button to close the application.");

                string value = this.reader.ReadLine();
                switch (value)
                {
                    case "1": this.BuyItem(); break;
                    case "2": this.ShowCharackterStatus(); break;
                    default: isPlaying = false; break;
                }
            }
        }

        private void RegisterPlayer()
        {
            this.writer.ClearConsole();
            while (true)// Register Character
            {
                this.writer.WriteLine("Select number of the heroe:" + Environment.NewLine +
                                       "1.Warrior" + Environment.NewLine +
                                       "2.Wizzard" + Environment.NewLine +
                                       "3.Rogue");
                bool correctHeroType = int.TryParse(this.reader.ReadLine(), out int result);
                if (!correctHeroType)
                {
                    this.InputValidation("Type");
                    continue;
                }

                string type = string.Empty;
                switch (result)
                {
                    case 1: type = "Warrior"; break;
                    case 2: type = "Wizzard"; break;
                    case 3: type = "Rogue"; break;
                }

                this.writer.Write("Select Name: ");
                string name = this.reader.ReadLine();
                if (string.IsNullOrWhiteSpace(name))
                {
                    this.InputValidation("Name");
                    continue;
                }

                this.repo.AddPlayer(this.factory.CreatePlayer(name, type));
                break;
            }
        }

        private void RegisterShopAndInitialWeapons()
        {
            this.repo.AddShop(this.factory.CreateShop());

            var weponI = this.factory.CreateWeapon("GreatswordI", "Greatsword");
            var weponII = this.factory.CreateWeapon("GreatswordII", "Greatsword");
            var weponIII = this.factory.CreateWeapon("GreatswordIII", "Greatsword");
            var weponIV = this.factory.CreateWeapon("SwordI", "Sword");
            var weponV = this.factory.CreateWeapon("SwordII", "Sword");
            var weponVI = this.factory.CreateWeapon("SwordIII", "Sword");
            var weponVII = this.factory.CreateWeapon("RapierI", "Rapier");
            var weponVIII = this.factory.CreateWeapon("RapierII", "Rapier");
            var weponVIX = this.factory.CreateWeapon("RapierIII", "Rapier");

            this.writer.WriteLine(Environment.NewLine + "Loading weapons in the store");
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponI));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponII));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponIII));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponIV));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponV));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponVI));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponVII));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponVIII));
            this.writer.WriteLine(this.repo.Shop.AddWeaponInStore(weponVIX));
            this.writer.WriteLine(null);
        }

        private void BuyItem()
        {
            this.writer.ClearConsole();

            IPlayer player = null;
            while (true)
            {
                string name = string.Empty;
                while (true)
                {
                    this.writer.Write("LogIn Name: ");
                    name = this.reader.ReadLine();
                    if (string.IsNullOrWhiteSpace(name))
                    {
                        this.InputValidation("Name");
                        continue;
                    }

                    break;
                }

                player = this.repo.TakePlayer(name);
                if (player == null)
                {
                    this.InputValidation("Name");
                    continue;
                }

                break;
            }

            while (true)
            {
                this.writer.WriteLine("Show all weapons in the store:");
                IShop shop = this.repo.Shop;
                int index = 1;
                this.PrintWeapon(shop, ref index);
                this.writer.WriteLine("Select number for weapon which want to but");
                bool IsCorrectNumber = int.TryParse(this.reader.ReadLine(), out int number);
                if (!IsCorrectNumber)
                {
                    this.InputValidation("Weapon number");
                    continue;
                }
                if (number > index - 1)
                {
                    this.writer.WriteLine("Please, select weapon again!!!");
                    continue;
                }

                IWeapon weapon = shop.Weapons.ToList()[number - 1];
                try
                {
                    player.AddWeapon(weapon);
                }
                catch (Exception ex)
                {
                    this.writer.WriteLine(ex.Message);
                    continue;
                }
                shop.SellWeapon(weapon);
                break;
            }
        }

        private void ShowCharackterStatus()
        {
            IPlayer player = null;
            while (true)
            {
                string name = string.Empty;
                while (true)
                {
                    this.writer.Write("Select Name: ");
                    name = this.reader.ReadLine();
                    if (string.IsNullOrWhiteSpace(name))
                    {
                        this.InputValidation("Name");
                        continue;
                    }

                    break;
                }

                player = this.repo.TakePlayer(name);
                if (player == null)
                {
                    this.InputValidation("Name");
                    continue;
                }

                break;
            }

            this.writer.WriteLine(player.ToString());
        }

        private void PrintWeapon(IShop shop, ref int index)
        {
            IEnumerable<IWeapon> weapons = shop.Catalogue();
            StringBuilder sb = new StringBuilder();
            foreach (var item in weapons)
            {
                sb.AppendLine("-----");
                sb.AppendLine($"{index++}# {item.ToString()}");
            }
            this.writer.WriteLine(sb.ToString().TrimEnd());
        }

        private void InputValidation(string parameter)
        {
            this.writer.ClearConsole();
            this.writer.WriteLine($"{parameter} cannot be null or empty!!!");
            this.writer.WriteLine("Repat again!!!");
        }
    }
}