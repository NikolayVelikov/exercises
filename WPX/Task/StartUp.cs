﻿using Weapon_Shop.Core;
using Weapon_Shop.Constraints;
using Weapon_Shop.Core.Contracts;

namespace Task
{
    public class StartUp
    {
        public static void Main(string[] args)
        {
            IFactory factory = new Factory(Parameters.InitialMoney);
            IRepository repo = new Repository();
            IWriter writer = new ConsoleWriter();
            IReader reader = new ConsoleReader();

            IEngine engine = new Engine(factory, repo, writer, reader);
            engine.Run();
        }
    }
}